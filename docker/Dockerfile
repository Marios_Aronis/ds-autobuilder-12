FROM ubuntu:18.04

# Install all the Linux packages required for Yocto builds. Note that the packages python3,
# tar, locales and cpio are not listed in the official Yocto documentation. The build, however,
# without them.
RUN apt-get update && apt-get -y install gawk wget git-core diffstat unzip texinfo gcc-multilib \
     build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
     python3-testtools python3-subunit \
     xz-utils debianutils iputils-ping libsdl1.2-dev xterm tar locales vim python3-pip virtualenv enchant \
     npm supervisor iproute2 wine-stable sudo rsync tightvncserver qemu-system-x86 libcppunit-subunit-dev \
     openvpn curl && apt-get clean && \
     rm -rf /var/lib/apt/lists/*

RUN  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - &&  echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && apt-get update && apt-get -y install yarn && apt-get clean && \
     rm -rf /var/lib/apt/lists/*
RUN pip3 install buildbot buildbot-www buildbot-waterfall-view buildbot-console-view buildbot-grid-view buildbot-worker buildbot_pkg
# By default, Ubuntu uses dash as an alias for sh. Dash does not support the source command
# needed for setting up the build environment in CMD. Use bash as an alias for sh.
RUN rm /bin/sh && ln -s bash /bin/sh && mkdir -p /var/log/supervisor && \
  mkdir -p /etc/supervisor/conf.d
#copy mender enterprise files
COPY mender-binary-delta-1.1.0.tgz /root/
RUN mkdir -p /opt && cd /opt && tar xvzf /root/mender-binary-delta-1.1.0.tgz && rm  /root/mender-binary-delta-1.1.0.tgz 
RUN mkdir -p /opt/mender-binary-delta-1.1.0/keys
# Set the locale to en_US.UTF-8, because the Yocto build fails without any locale set.
RUN locale-gen en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

ENV USER_NAME autobuilder
ENV PROJECT ds-os

# The running container writes all the build artefacts to a host directory (outside the container).
# The container can only write files to host directories, if it uses the same user ID and
# group ID owning the host directories. The host_uid and group_uid are passed to the docker build
# command with the --build-arg option. By default, they are both 1001. The docker image creates
# a group with host_gid and a user with host_uid and adds the user to the group. The symbolic
# name of the group and user is cuteradio.
ARG host_uid=1001
ARG host_gid=1001
RUN groupadd -g $host_gid $USER_NAME && useradd -g $host_gid -m -s /bin/bash -u $host_uid $USER_NAME

# Perform the Yocto build as user cuteradio (not as root).
# NOTE: The USER command does not set the environment variable HOME.

# By default, docker runs as root. However, Yocto builds should not be run as root, but as a 
# normal user. Hence, we switch to the newly created user cuteradio.

# Create the directory structure for the Yocto build in the container. The lowest two directory
# levels must be the same as on the host.
ENV BUILD_INPUT_DIR /home/$USER_NAME/yocto/input
ENV BUILD_OUTPUT_DIR /home/$USER_NAME/yocto/output
RUN mkdir -p $BUILD_INPUT_DIR $BUILD_OUTPUT_DIR
RUN mkdir -p /home/${USER_NAME}/.ssh
COPY keys/id_rsa /home/${USER_NAME}/.ssh/
COPY keys/id_rsa.pub /home/${USER_NAME}/.ssh/
RUN chmod 600 /home/${USER_NAME}/.ssh/id_rsa
RUN chown -R ${USER_NAME} /home/${USER_NAME}

USER $USER_NAME

# Clone the repositories of the meta layers into the directory $BUILD_INPUT_DIR/sources/cuteradio.
WORKDIR $BUILD_INPUT_DIR
RUN ssh-keyscan -t rsa bitbucket.org > /home/${USER_NAME}/.ssh/known_hosts
#RUN true
#RUN git clone --recurse-submodules ssh://git@bitbucket.org/deepseaiot/$PROJECT.git

RUN mkdir -p /home/${USER_NAME}/pokybuild3
WORKDIR /home/${USER_NAME}/pokybuild3
RUN buildbot create-master -r yocto-controller
#RUN cd yocto-controller && git clone https://git.yoctoproject.org/git/yocto-autobuilder2 yoctoabb \
#     && cd yoctoabb && git checkout 9fa3dee873f43f0ae1d836254c7fb4576f7b3ae6 \
#
ARG AB_REV=warrior
RUN cd yocto-controller && git clone ssh://git@bitbucket.org/deepseaiot/ds-autobuilder.git yoctoabb \
     && cd yoctoabb && git checkout ${AB_REV} \
     && cd .. && ln -rs yoctoabb/master.cfg master.cfg 
### RUN ln -s yocto-controller-template yocto-controller
COPY main.module.coffee yocto-controller/yoctoabb/yocto_console_view/src/module/
RUN cd yocto-controller/yoctoabb/yocto_console_view && python3 setup.py build

ARG POKY_BRANCH=warrior
RUN git clone https://git.yoctoproject.org/git/poky --depth 1 --branch ${POKY_BRANCH} --single-branch
#COPY buildbot.tac /home/${USER_NAME}/pokybuild3/yocto-worker/
RUN buildbot-worker create-worker -r --umask=0o22 yocto-worker-template ds-autobuilder-controller ubuntu1804-ty-1 pass
RUN mkdir -p yocto-worker-template/repo-stash-dir
RUN mkdir -p yocto-worker-template/trash
### RUN ln -s yocto-worker-template yocto-worker
USER ${USER_NAME}
WORKDIR /home/${USER_NAME}
RUN true
ARG AB_HELPER_REV=warrior
#RUN git clone https://git.yoctoproject.org/git/yocto-autobuilder-helper && cd yocto-autobuilder-helper && git checkout $HELPER_REV
RUN git clone git@bitbucket.org:deepseaiot/yocto-autobuilder-helper.git && cd yocto-autobuilder-helper && git checkout $AB_HELPER_REV
#RUN git clone git@bitbucket.org:deepseaiot/yocto-autobuilder-helper.git && cd yocto-autobuilder-helper && git checkout 4bf90e2e8ffb7871c043ede7529739bbe5e4105b
USER root
##RUN mkdir /root/.ssh
##COPY id_rsa /root/.ssh
##RUN chmod 600 /root/.ssh/id_rsa
##RUN ssh-keyscan -t rsa bitbucket.org > /root/.ssh/known_hosts
#RUN cd /root && git clone https://git.yoctoproject.org/git/yocto-autobuilder-helper
#RUN cd /root && git clone git@bitbucket.org:deepseaiot/yocto-autobuilder-helper.git && cd yocto-autobuilder-helper && git checkout $BRANCH
RUN cd /root && ln -s /home/${USER_NAME}/yocto-autobuilder-helper
#must also be done in init-dirs
RUN mkdir -p /srv/autobuilder/dsautobuilder-share/repos
RUN mkdir -p /srv/autobuilder/dsautobuilder-share/current_sources
RUN mkdir -p /srv/autobuilder/dsautobuilder-share/pub
RUN mkdir -p /srv/autobuilder/dsautobuilder-pub
# RUN mkdir -p /srv/www/vhosts
# RUN chown -R ${USER_NAME} /srv/www/vhosts


COPY supervisor.conf /etc/supervisor.conf
COPY supervisor-worker.conf /etc/supervisor-worker.conf
COPY supervisor-controller.conf /etc/supervisor-controller.conf
USER ${USER_NAME}
#COPY yoctoabb/master.cfg /home/${USER_NAME}/pokybuild3/yocto-controller/yoctoabb/
#COPY yoctoabb/config.py /home/${USER_NAME}/pokybuild3/yocto-controller/yoctoabb/
#COPY yoctoabb/schedulers.py /home/${USER_NAME}/pokybuild3/yocto-controller/yoctoabb/
#COPY yoctoabb/builders.py /home/${USER_NAME}/pokybuild3/yocto-controller/yoctoabb/
RUN cp -ar /home/${USER_NAME}/pokybuild3/yocto-controller /home/${USER_NAME}/pokybuild3/yocto-controller-template
COPY keys/private.key /opt/mender-binary-delta-1.1.0/keys
COPY keys/public.key /opt/mender-binary-delta-1.1.0/keys
RUN mkdir -p /home/${USER_NAME}/.vnc
COPY keys/passwd /home/${USER_NAME}/.vnc/
USER root
RUN chown ${USER_NAME} /home/${USER_NAME}/.vnc/passwd
RUN chmod 600 /home/${USER_NAME}/.vnc/passwd
RUN chown -R ${USER_NAME} /srv/autobuilder
RUN chown -R ${USER_NAME} /root/yocto-autobuilder-helper
RUN chown -R ${USER_NAME} /opt/mender-binary-delta-1.1.0/keys
#COPY config.json /root/yocto-autobuilder-helper/
COPY scripts/bitbake-hashserv.sh /usr/bin
COPY scripts/bitbake-worker.sh /usr/bin
COPY scripts/bitbake-controller.sh /usr/bin
COPY scripts/init.sh /usr/bin
COPY scripts/init-dirs.sh /usr/bin
RUN chmod a+rx /usr/bin/bitbake-hashserv.sh
RUN chmod a+rx /usr/bin/bitbake-worker.sh
RUN chmod a+rx /usr/bin/bitbake-controller.sh
RUN chmod a+rx /usr/bin/init-dirs.sh
RUN chmod a+rx /usr/bin/init.sh
RUN chmod a+rx /root
ENV AB_MODE=controller
ENV AB_CONTROLLER_ADDR=ds-autobuilder-controller
ENV AB_WEBSERVER_ADDR=ds-autobuilder-webserver
ENV AB_CONTROLLER_PORT=9989
ENV AB_WORKER_NAME=ubuntu1804-ty-1
CMD ["/usr/bin/init.sh"]

VOLUME /srv/autobuilder/dsautobuilder-share
VOLUME /srv/autobuilder/dsautobuilder-pub
VOLUME /home/autobuilder/pokybuild3/yocto-worker
VOLUME /home/autobuilder/pokybuild3/yocto-controller
VOLUME /opt/mender-binary-delta-1.1.0/keys
# Prepare Yocto's build environment. If TEMPLATECONF is set, the script oe-init-build-env will
# install the customised files bblayers.conf and local.conf. This script initialises the Yocto
# build environment. The bitbake command builds the rootfs for our embedded device.
#WORKDIR $BUILD_OUTPUT_DIR
#ENV TEMPLATECONF=$BUILD_INPUT_DIR/$PROJECT/sources/meta-ds/conf
#CMD source $BUILD_INPUT_DIR/$PROJECT/sources/poky/oe-init-build-env build \
#    && bitbake core-image-base-ds
#COPY dskey /home/${USER_NAME}/.ssh/id_rsa
#USER root
#RUN chown ${USER_NAME} /home/${USER_NAME}/.ssh/id_rsa
#USER $USER_NAME
EXPOSE 8010
EXPOSE 9989
EXPOSE 8686



